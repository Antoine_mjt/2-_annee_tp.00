# NE PAS METTRE D'ACCENTS DANS LES COMMENTAIRES !
def say_hello():
    """
    This function only say hello
    Nothing less, nothing more
    But this is a cute doc string.
    """
    print("Hello World") # What a complex line of code
    
    
# nombre entier : int & long
# reels : float
# complexes : complex
# string : str
# unicode : unicode
# booleens : bool
    
# pour connaitre le type d'une variable il existe la fonction
# type()
    
name = 'ben'
print(type(name))

    
# voici un exemple de cast ci-dessous
    
a = '4'
print(type(a))
int_a = int(a)
type(int_a)

# exemple de listes

ints = [0, 1, 1, 2, 2, 3, 5, 8, 13] # initialisation de la liste
print(ints)
print(type(ints))
print(ints[6]) # affichage de la 6eme case de la liste (ici 5)


ints[6] = 'Hello' # la 6eme case prend la valeur donne (ici 5 est remplace par 'Hello')       
print(ints)
ints.pop(4) # la 4eme case est supprime (ici le 2eme '2' disparait)
print(ints)

# dans une liste la propriete .pop supprime la case demande 


ints.append(42) # ajouts d'une case supplementaire qui prend la valeur defini (ici 42)
print(ints)
other = ['etc.'] # creation d'une liste 'other' possedant une case ayant la valeur 'etc.'
print(ints + other) # concatenation des listes ints et other
print(ints[2:]) # on affiche la liste a partir de la 2eme case (ici le 2eme '1')
print(len(ints)) # la fonction len() compte le nombre de case de la liste choisie (ici ints)

# la propriete .append permet d'ajouter une nouvelle case a la liste avec une valeur choisie
# on peut concatener des listes entre elles car elles sont de types 'list', 
# peu importe si elles possedent des entiers, des reels ou des chaines de caracteres;


# exemple de creation de liste avec des boucles for

chars = {                                                   # chars est un dictionnaire
    'luke': {'job': 'jedi', 'name': 'Luke Skywalker', },    # luke, han et ben sont des listes
    'han': {'job': 'smuggler', 'name': 'Han Solo', },       # job et name sont des cles
    'ben': {'job': 'jedi', 'name': 'Ben Kenobi', },
}

print([value['name'] for key, value in chars.items() if value['job'] == 'jedi'])    
# on demande d'afficher la valeur de la cle 'name' pour chaque liste du dictionnaire chars si la valeur de la cle 'job' est 'jedi' 


# exemple de creation de tuple

jobs = ('jedi', 'smuggler', 'farmer', ) # creation de tuple

print(jobs) 
print(jobs[1]) # on affiche la case '1' du tuple jobs

print('jedi' in jobs) # on demande si jedi est dans jobs et si oui on affichera 'true' si non on affichera 'false'
# jobs.pop(1)  on nous indique que cette commande est impossible et c'est normal car on ne peut pas modifier un tuple


# exemple d'utilisation de dictionnaires

chars = {
    'luke': {'job': 'jedi', 'name': 'Luke Skywalker', },
    'han': {'job': 'smuggler', 'name': 'Han Solo', },
    'ben': {'job': 'jedi', 'name': 'Ben Kenobi', },
}

print(chars['luke']) # on affiche la liste de luke
print(chars['luke']['name']) # on affiche la valeur de 'name' de la liste 'luke'

chars = ['BEN', 'Luke', 'han']

print({c.lower(): len(c) for c in chars}) # on affiche le nombre de caractere de chaque prenoms dans l'odre decroissant


# exemple d'utilisation des if, elif et else


age = int(input('What\'s your age ?')) # on affiche la question 'What's your age ? puis on le saisi 

if age < 16:
    print("Go to school!")
elif age >= 60:
    print("Retirement times!")
else:
    print("Go to works (or school ?)!")
    

# exemple de bouclefor name in ['ben', 'han', 'luke']:
    print(name)

for i in range(10):
    print(i)
    
for name in ['ben', 'han', 'luke']:
    print(name)

for i in range(10):
    print(i)
    
compteur = 10

while compteur > 0:
    print ("T- {nombre}".format(nombre=compteur))
    compteur = compteur  - 1

# exemple d'utilisation de fonction

def say_hello(name):
    print("Hello %s" % (name))
    
    
# L'addition (+)
# La soustraction (-)
# La multiplication (*)
# La division (/)
# Le modulo (%)
# La puissance (**)

# egalite (==)
# Non egalite (!=)
# Inferieur, inferieur ou egal (<, <=)
# Superieur, superieur ou egal (>, >=)

# conjonctions d'expression and, or, not
